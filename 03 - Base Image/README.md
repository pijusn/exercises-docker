Implement base image to be used by multiple Node application. There is an empty Dockerfile to be filled-in.

This is a common pattern for teams managing multiple services utilizing similar tech-stacks.

It should all work when executing command:
```shell
docker build --tag=base ./base
docker build --tag=app-a ./app-a
docker build --tag=app-b ./app-b
sleep && docer ps
```

## Walkthrough

<details>

  ```Dockerfile
  FROM node:17

  WORKDIR /app
  ENTRYPOINT [ "npm", "start" ]

  ONBUILD COPY package.json .
  ONBUILD RUN npm install

  ONBUILD COPY src/ src/
  ```

  Compared to building a typical Node application image, the key is usage of `ONBUILD` command. It defines command to be executed at the beginning of image build extending current image. In this case, it will execute those commands when building `app-a` and `app-b`.
</details>
