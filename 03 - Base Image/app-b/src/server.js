const Express = require("express")
const app = Express()
const port = process.env.port || 3000

app.get("/", (req, res) => {
  res.send("This is application B.")
})

app.listen(port, () => {
  console.log(`Example app listening on http://localhost:${port}/`)
})
