# Preparation

## Installation

Follow [official instructions](https://docs.docker.com/get-docker/).

## Verification

```shell
docker run chuanwen/cowsay
```

You might have to wait a little but if you see the cow eventually, you should be fine.

## Preloading

While Docker will fetch everything needed as you go, you can run following commands to prefech some resources that will or could be used in the exercises.

```shell
docker pull debian
docker pull node
docker pull golang
docker pull redis
docker pull rediscommander/redis-commander
```
