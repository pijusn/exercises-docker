# Excercises: Docker 

This is a set of excercises to learn basics of using Docker. Go through [preparation](./00%20-%20Preparation) in advance.

<details>
  <summary>Useful commands to kick-you off</summary>

  `docker build --tag=<tag> .` Build new image with tag specified.

  `docker images` List tagged images.

  `docker images -a` List all images.

  `docker run --detach <id/tag>` Run container in detached mode from image specified.

  `docker ps` List running containers.

  `docker ps -a` List all containers.

  `docker rm <id/name>` Remove container specified.

  `docker network ls` List networks.

  `docker network create <name>` Create new network.

  `docker network rm <id/name>` Remove network specified.
<details>
