Run Redis (database) and Redis Commander (interface). Connect them together.

Yes, you will have to [search](https://hub.docker.com/) for stuff.

## Walkthrough

<details>

  ```shell
  docker network create redis-network
  ```

  Create a network. Not required but using networks is generally the better practice. Alternatively, you could use container linking which would be simpler in this case specifically.

  Putting containers on the same network allows them to connect each other.

  ```shell
  docker run --detach --name=redis --network=redis-network redis
  ```

  `--name=redis` names the container. Importantly, it is used as a hostname in the network.

  Port `6379` is exposed by Redis by default but since we are not publishing it, it will only be accessible to the containers on the same network.

  ```shell
  docker run --detach --network=redis-network --env=REDIS_HOSTS=local:redis:6379 --publish=8081:8081 rediscommander/redis-commander
  ```

  `--env=REDIS_HOSTS=local:redis:6379` lets Redis Commander know where Redis is. Variable is specific to the implementation of Redis Commander.

  `--publish=8081:8081` publishes default Redis Commander port so that we could connect to it.

  Redis Commander should be accessible via http://localhost:8081 . There isn't much else to do as long as it opens up.
</details>
