Build `app` as a Docker image. There is an empty Dockerfile - fill it in.

Hint: official `node` image (if used) will require changing working directory for `npm install` to function.

## Walkthrough

<details>

  `Dockerfile`:
  ```Dockerfile
  FROM node:17

  WORKDIR /app
  COPY package.json .
  RUN npm install

  COPY src/ src/

  ENTRYPOINT [ "npm", "start" ]
  ```

  `WORKDIR` is only needed because of how `node` image is set up. `npm install` fails otherwise. It's still best to structure files properly but it's a little annoying for the examaple purposes.

  The rest is basically as described by the README.

  Command:
  ```shell
  docker build -t . && docker run -it app -p 3000:3000
  ```

  `app` is an arbitrary tag we give to the image so that we can refer to it more easily.

  `-it` flag runs conainer in interactive mode allowing to interupt it using `ctrl`+`c`.

  By default, containers do not expose any ports. Argument `-p 3000:3000` specifies to map port `3000` on the machine to port `3000` on the container.

  Bear in mind, order of arguments is important.

  Application should be accessible via http://localhost:3000 . It doesn't have much.
</details>
