package main

import (
	"fmt"

	"github.com/brianvoe/gofakeit/v6"
)

func main() {
	fmt.Println("You did it! Here is a random sentence:")
	fmt.Println(gofakeit.SentenceSimple())
}
