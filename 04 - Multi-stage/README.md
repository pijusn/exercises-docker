Build example application with Docker multi-stage build. There is an empty Dockerfile - fill it in.

It should all work when executing command:
```sh
docker build --tag=e04 ./app && docker run e04
```

## Walkthrough

<details>

  ```Dockerfile
  FROM golang
  WORKDIR /build
  COPY go.mod .
  COPY go.sum .
  RUN go mod download
  COPY main.go .
  RUN go build -o app .

  FROM debian
  WORKDIR /app
  COPY --from=0 /build/app .
  CMD ["./app"] 
  ```

  It is effectively two Dockerfiles concatenated. One defines the build process, the other defined the image of the application. Neither of them has anything special about them. The only unusual part is the use of `--from=0` where `0` refers to the first image.
</details>
